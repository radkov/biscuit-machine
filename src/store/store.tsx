import { applyMiddleware, combineReducers, createStore, Store } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';

import { switchReducer } from '../reducers/switchReducer';
import { ISwitchState } from '../interfaces/switchInterface';

export interface IAppState {
    switch: ISwitchState;
    // conveyor: IConveyorState;
    // oven: IOvenState;
    // motor: IMotorState;
    // extruder: IExtruderState;
    // Stamper: IStamperState;
}

const rootReducer = combineReducers<IAppState>({
    switch: switchReducer,
});

export default function configureStore(): Store<IAppState, any> {
    const store = createStore(rootReducer, undefined, composeWithDevTools(applyMiddleware()));
    return store;
}
