import { ActionCreator } from 'redux';

export enum SwitchMode {
    On = 'on',
    Off = 'off',
    Paused = 'paused',
}

export interface ISwitchState {
    mode: SwitchMode;
}

export enum SwitchActionTypes {
    ChangeMode = 'ChangeMode',
}

export interface ISwitchAnyAction {
    type: SwitchActionTypes.ChangeMode;
    mode: SwitchMode;
}

export type changeSwitchModeActionType = ActionCreator<ISwitchState>;
