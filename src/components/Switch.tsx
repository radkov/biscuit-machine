import React from 'react';
import { SwitchMode, changeSwitchModeActionType } from '../interfaces/switchInterface';

interface ISwitchProps {
    mode: SwitchMode;
    changeMode: changeSwitchModeActionType;
}

const Switch = ({ mode, changeMode }: ISwitchProps) => {
    return (
        <div className="switch-toggle switch-3 switch-candy">
            <input
                id="on"
                name="state-d"
                type="radio"
                defaultChecked={mode === SwitchMode.On}
                disabled={mode === SwitchMode.Off}
            />
            <label htmlFor="on" onClick={() => changeMode(SwitchMode.On)}>
                ON
            </label>

            <input id="na" name="state-d" type="radio" defaultChecked={mode === SwitchMode.Paused} />
            <label htmlFor="na" className="disabled" onClick={() => changeMode(SwitchMode.Paused)}>
                Pause
            </label>

            <input
                id="off"
                name="state-d"
                type="radio"
                defaultChecked={mode === SwitchMode.Off}
                disabled={mode === SwitchMode.On}
            />
            <label htmlFor="off" onClick={() => changeMode(SwitchMode.Off)}>
                OFF
            </label>
        </div>
    );
};

export default Switch;
