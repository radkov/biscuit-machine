import React, { ComponentType } from 'react';
import { connect, Matching } from 'react-redux';
import { ActionCreator } from 'redux';

import './App.css';
import Switch from './components/Switch';
import { IAppState } from './store/store';
import { changeSwitchModeAction } from './actions/switchActions';
import { SwitchMode, changeSwitchModeActionType } from './interfaces/switchInterface';

interface IAppProps {
    switchMode: SwitchMode;
    changeSwitchModeAction: changeSwitchModeActionType;
}

const App: React.FC<IAppProps> = ({ switchMode, changeSwitchModeAction }: IAppProps) => {
    return <div className="App">{<Switch mode={switchMode} changeMode={changeSwitchModeAction} />}</div>;
};

const mapStateToProps = (state: IAppState) => {
    return {
        switchMode: state.switch.mode,
    };
};

const mapDispatchToProps = { changeSwitchModeAction };

export default connect(mapStateToProps, mapDispatchToProps)(App);
