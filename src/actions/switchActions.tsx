import { Dispatch } from 'redux';

import { SwitchActionTypes, SwitchMode, changeSwitchModeActionType } from '../interfaces/switchInterface';

export const changeSwitchModeAction: changeSwitchModeActionType = (mode: SwitchMode) => {
    return {
        mode,
        type: SwitchActionTypes.ChangeMode,
    };
};
