import { Reducer } from 'redux';
import { SwitchActionTypes, SwitchMode, ISwitchAnyAction } from '../interfaces/switchInterface';
import { ISwitchState } from '../interfaces/switchInterface';

const initialSwitchState: ISwitchState = {
    mode: SwitchMode.Off,
};

export const switchReducer: Reducer<ISwitchState, ISwitchAnyAction> = (state = initialSwitchState, action) => {
    switch (action.type) {
        case SwitchActionTypes.ChangeMode: {
            return {
                ...state,
                mode: action.mode,
            };
        }
        default:
            return state;
    }
};
